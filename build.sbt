name := "scalacron"

version := "0.1"

scalaVersion := "3.0.2"

idePackagePrefix := Some("dev.slyk")

enablePlugins(NativeImagePlugin)
nativeImageOptions ++= List(
  "--no-fallback"
)

libraryDependencies += "org.scalameta" %% "munit" % "0.7.27"
testFrameworks += new TestFramework("munit.Framework")