# Scalacron
CRON parser written with Scala 3.

## Requirements
- [sbt](https://www.scala-sbt.org/download.html)
- Java 8

## Setup
### On WSL2
1. Install [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
2. Install [Ubuntu from MS Store](https://www.microsoft.com/en-us/p/ubuntu/9nblggh4msv6?activetab=pivot:overviewtab)
3. Search for Ubuntu on your machine; it will open a terminal
4. Follow the steps below

### On a clean machine (Ubuntu)
1. Install SDKMan  
```bash
sudo apt install zip unzip
curl -s "https://get.sdkman.io" | bash
```
2. Install Java and sbt
```bash
sdk install java $(sdk list java | grep -o "8\.[0-9]*\.[0-9]*\.hs-adpt" | head -1)
sdk install sbt
```
3. Install native-image compilation dependencies
```bash
sudo apt install build-essential libtool zlib1g-dev
```

## Running the native app
**These steps have to be performed on Linux/MacOS.**
1. Open a terminal window in the project directory
2. Compile the Native Image using GraalVM plugin:
```bash
sbt nativeImage
```
2. Run the compiled output
```bash
target/native-image/scalacron "*/15 0 1,15 * 1-5 /usr/bin/find"
```
You can also time the command to check how long it took to run like this:
```bash
time target/native-image/scalacron "*/15 0 1,15 * 1-5 /usr/bin/find"
```

[Technology Choices](docs/Technology%20Choices.md)

## Notes
Cats `Either#sequence` method seems to cause issues in the native image variant
of the application. While there is a workaround for this it seems very elaborate
and would take some time to document and/or package nicely in this repository for
reproducible results. For this reason, and considering the very limited use of Cats
I have elected to remove it from the codebase and sidestep the reflection issues
altogether.
