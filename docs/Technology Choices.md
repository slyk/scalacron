# Choice of Technologies for this project

## Main Language

### Scala 3

Strongly, statically typed language that allows mixing Functional Programming and Object-Oriented programming paradigms.
JVM-based, cross-platform.

Requires a moderately heavy setup. Small scripts are disproportionately affected by this. JVM is slow to start, has
warm-up time until it reaches optimal performance.

### JavaScript

Much like TypeScript without the advantage of type system during development.

Pros:

- Extremely rich library ecosystem, there's a library for everything
- Widely supported
- Relatively lightweight setup
- Does not have JVM warm-up time

Cons:

- Dependency tree tends to be very large and elaborate
- No type system

### Typescript

Static type layer on top of JavaScript. Allows developing as though JavaScript had a type system. Transpiles down to
JavaScript, meaning there is no type system at runtime.

### Bash

Pros:

- Runs natively on Linux/MacOS.
- Requires no setup
- Small footprint

Cons:

- Hard to use for larger scripts/applications
- No type system
- No dependency management system.

## Build Tools

### [SBT](https://www.scala-sbt.org/)

Pros:

- Allows writing build files in Scala
- Widely adopted
- Relatively rich plugin ecosystem
- Supports Scala 3

Cons:

- Terribly cryptic symbolic syntax
- Documentation only goes so far; more advanced use cases are undocumented and require reading the source code to
  implement (e.g. replacing the resolvers SBT uses to install itself)
- Forces its DSL and domain model on you very quickly
- Has a tendency to become very slow and freeze IDEs in larger projects
- Interpretation model makes it difficult to trace things through the IDE (e.g. ctrl+click doesn't take you where you
  want to go)
- [Long list of practical complaints from the community](https://www.lihaoyi.com/post/SowhatswrongwithSBT.html)

### [Mill](https://com-lihaoyi.github.io/mill/mill/Intro_to_Mill.html)

Pros:

- Allows writing build files in Scala
- Does not use cryptic syntax like SBT
- Tasks are just functions
- Supports Scala 3

Cons:

- Lacklustre plugin ecosystem
- Not widely adopted

### [Gradle](https://gradle.org/)

Pros:

- Widely supported
- Well documented
- Supports multiple languages
- Many plugins available

Cons:

- Introduces either Groovy which is dynamically typed and very different to Scala, or Kotlin which while similar to
  Scala in some ways has several important syntactical differences
- Does not allow writing build files in Scala
- Does not support Scala 3, which means incurring technical debt immediately

## Native Packaging

### Scala Native

Scala Native currently does not support Scala 3.

### GraalVM

Supports Scala 3.  
Has a plugin for SBT.