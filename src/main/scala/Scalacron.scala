package dev.slyk

import cron.{Formatter, Parser}

/**
 * @example *&#47;15 0 1,15 * 1-5 /usr/bin/find
 * */
@main def parseCommandLine(input: String*): Unit =
  if (input.isEmpty)
    println("Please provide a string input")
  else if (input.length == 1)
    Parser.parse(input(0)) match {
      case Left(error) =>
        println(error)
      case Right(cronExpr) =>
        println(Formatter.prettyFormat(cronExpr))
    }
  else
    println("Please provide your input surrounded by quotation marks (\") like this: \"*/15 0 1,15 * 1-5 /usr/bin/find\"")
