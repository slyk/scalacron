package dev.slyk
package utils

extension[L, R] (eitherList: List[Either[L, R]]) {
  def sequence: Either[L, List[R]] = {
    val (left, right) = eitherList.partitionMap(identity)
    left.headOption.toLeft(right)
  }
}
