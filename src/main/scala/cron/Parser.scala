package dev.slyk
package cron

import cron.model.*

object Parser {

  /**
   * @example *&#47;15 0 1,15 * 1-5
   * */
  def parse(input: String): Either[CronParsingError, CronExpression] = {
    val elements = input.split(' ')
    println(elements.toList)
    if (elements.length != 6) Left(InvalidNumberOfCronExpressionElements)
    else for {
      minutes <- CronMinutes(elements(0))
      hours <- CronHours(elements(1))
      daysOfMonth <- CronDaysOfMonth(elements(2))
      months <- CronMonths(elements(3))
      daysOfWeek <- CronDaysOfWeek(elements(4))
      command = elements(5)
    } yield CronExpression(minutes, hours, daysOfMonth, months, daysOfWeek, command)
  }

}
