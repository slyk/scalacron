package dev.slyk
package cron.model

case class CronExpression(
  minutes: CronMinutes,
  hours: CronHours,
  daysOfMonth: CronDaysOfMonth,
  months: CronMonths,
  daysOfWeek: CronDaysOfWeek,
  command: String
)
