package dev.slyk
package cron.model

import utils.sequence

sealed trait CronDaysOfMonth

case class CronDaysOfMonthWildcard() extends CronDaysOfMonth

case class CronFixedDaysOfMonth(value: Int) extends CronDaysOfMonth

case class CronDaysOfMonthList(values: List[Int]) extends CronDaysOfMonth

case class CronDaysOfMonthRange(start: Int, end: Int) extends CronDaysOfMonth

case class CronDaysOfMonthSteps(step: Int) extends CronDaysOfMonth

case class CronDaysOfMonthStepsWithRange(range: CronDaysOfMonthRange, step: Int) extends CronDaysOfMonth

object CronDaysOfMonth {
  type ValidatedDaysOfMonth[A <: CronDaysOfMonth] = Either[CronParsingError, A]

  def apply(cronDaysOfMonthParameter: String): ValidatedDaysOfMonth[CronDaysOfMonth] =
    isWildcard(cronDaysOfMonthParameter) orElse
      isValidDayOfMonthValue(cronDaysOfMonthParameter) orElse
      isValidDayOfMonthRange(cronDaysOfMonthParameter) orElse
      isValidStepValue(cronDaysOfMonthParameter) orElse
      isValidStepValueWithRange(cronDaysOfMonthParameter) orElse
      isValidDayOfMonthList(cronDaysOfMonthParameter)

  private def isValidDayOfMonthValue(string: String): Either[CronParsingError, CronFixedDaysOfMonth] =
    string
      .toIntOption
      .toRight(left = InvalidDaysOfMonthString)
      .filterOrElse(
        daysOfMonth => daysOfMonth >= 1 && daysOfMonth < 31,
        InvalidDaysOfMonthString
      )
      .map(CronFixedDaysOfMonth.apply)

  private def isValidDayOfMonthRange(string: String): ValidatedDaysOfMonth[CronDaysOfMonthRange] =
    string.split("-").toList match {
      case start :: end :: Nil =>
        for {
          validatedStart <- isValidDayOfMonthValue(start)
          validatedEnd <- isValidDayOfMonthValue(end)
        } yield CronDaysOfMonthRange(validatedStart.value, validatedEnd.value)
      case _ => Left(InvalidDaysOfMonthString)
    }

  private def isWildcard(string: String): ValidatedDaysOfMonth[CronDaysOfMonthWildcard] =
    Either.cond(
      test = string == "*",
      right = CronDaysOfMonthWildcard(),
      left = InvalidDaysOfMonthString,
    )

  private def isValidStepValueWithRange(string: String): ValidatedDaysOfMonth[CronDaysOfMonthStepsWithRange] =
    string.split("/").toList match {
      case range :: step :: Nil =>
        for {
          validatedRange <- isValidDayOfMonthRange(range)
          validatedStep <- isValidDayOfMonthValue(step)
        } yield CronDaysOfMonthStepsWithRange(validatedRange, validatedStep.value)
      case _ => Left(InvalidDaysOfMonthString)
    }

  private def isValidStepValue(string: String): ValidatedDaysOfMonth[CronDaysOfMonthSteps] =
    string.split("/").toList match {
      case range :: step :: Nil =>
        for {
          _ <- isWildcard(range)
          validatedStep <- isValidDayOfMonthValue(step)
        } yield CronDaysOfMonthSteps(validatedStep.value)
      case _ => Left(InvalidDaysOfMonthString)
    }

  private def isValidDayOfMonthList(string: String): ValidatedDaysOfMonth[CronDaysOfMonthList] =
    string.split(",")
      .toList
      .map(isValidDayOfMonthValue)
      .sequence
      .filterOrElse(
        _ => !string.endsWith(","), //this prevents input such as "1," or "," from being valid
        InvalidDaysOfMonthString
      )
      .map(list => CronDaysOfMonthList(list.map(_.value)))

}
