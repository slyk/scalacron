package dev.slyk
package cron.model

import utils.sequence

sealed trait CronHours

case class CronHoursWildcard() extends CronHours

case class CronFixedHours(value: Int) extends CronHours

case class CronHoursList(values: List[Int]) extends CronHours

case class CronHoursRange(start: Int, end: Int) extends CronHours

case class CronHoursSteps(step: Int) extends CronHours

case class CronHoursStepsWithRange(range: CronHoursRange, step: Int) extends CronHours

object CronHours {
  type ValidatedHours[A <: CronHours] = Either[CronParsingError, A]

  def apply(cronHoursParameter: String): ValidatedHours[CronHours] =
    isWildcard(cronHoursParameter) orElse
      isValidHourValue(cronHoursParameter) orElse
      isValidHourRange(cronHoursParameter) orElse
      isValidStepValue(cronHoursParameter) orElse
      isValidStepValueWithRange(cronHoursParameter) orElse
      isValidHourList(cronHoursParameter)

  private def isValidHourValue(string: String): Either[CronParsingError, CronFixedHours] =
    string
      .toIntOption
      .toRight(left = InvalidHoursString)
      .filterOrElse(
        hours => hours >= 0 && hours < 23,
        InvalidHoursString
      )
      .map(CronFixedHours.apply)

  private def isValidHourRange(string: String): ValidatedHours[CronHoursRange] =
    string.split("-").toList match {
      case start :: end :: Nil =>
        for {
          validatedStart <- isValidHourValue(start)
          validatedEnd <- isValidHourValue(end)
        } yield CronHoursRange(validatedStart.value, validatedEnd.value)
      case _ => Left(InvalidHoursString)
    }

  private def isWildcard(string: String): ValidatedHours[CronHoursWildcard] =
    Either.cond(
      test = string == "*",
      right = CronHoursWildcard(),
      left = InvalidHoursString,
    )

  private def isValidStepValueWithRange(string: String): ValidatedHours[CronHoursStepsWithRange] =
    string.split("/").toList match {
      case range :: step :: Nil =>
        for {
          validatedRange <- isValidHourRange(range)
          validatedStep <- isValidHourValue(step)
        } yield CronHoursStepsWithRange(validatedRange, validatedStep.value)
      case _ => Left(InvalidHoursString)
    }

  private def isValidStepValue(string: String): ValidatedHours[CronHoursSteps] =
    string.split("/").toList match {
      case range :: step :: Nil =>
        for {
          _ <- isWildcard(range)
          validatedStep <- isValidHourValue(step)
        } yield CronHoursSteps(validatedStep.value)
      case _ => Left(InvalidHoursString)
    }

  private def isValidHourList(string: String): ValidatedHours[CronHoursList] =
    string.split(",")
      .toList
      .map(isValidHourValue)
      .sequence
      .filterOrElse(
        _ => !string.endsWith(","), //this prevents input such as "1," or "," from being valid
        InvalidHoursString
      )
      .map(list => CronHoursList(list.map(_.value)))

}
