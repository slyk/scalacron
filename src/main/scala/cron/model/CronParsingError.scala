package dev.slyk
package cron.model

sealed trait CronParsingError

case object InvalidMinutesString extends CronParsingError

case object InvalidHoursString extends CronParsingError

case object InvalidDaysOfMonthString extends CronParsingError

case object InvalidDaysOfWeekString extends CronParsingError

case object InvalidMonthsString extends CronParsingError

case object InvalidNumberOfCronExpressionElements extends CronParsingError