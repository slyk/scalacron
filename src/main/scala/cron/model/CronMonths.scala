package dev.slyk
package cron.model

import utils.sequence

sealed trait CronMonths

case class CronMonthsWildcard() extends CronMonths

case class CronFixedMonths(value: Int) extends CronMonths

case class CronMonthsList(values: List[Int]) extends CronMonths

case class CronMonthsRange(start: Int, end: Int) extends CronMonths

case class CronMonthsSteps(step: Int) extends CronMonths

case class CronMonthsStepsWithRange(range: CronMonthsRange, step: Int) extends CronMonths

object CronMonths {

  type ValidatedMonths[A <: CronMonths] = Either[CronParsingError, A]

  private val months = List("jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec")

  def apply(cronMonthsParameter: String): ValidatedMonths[CronMonths] =
    isWildcard(cronMonthsParameter) orElse
      isValidMonthValue(cronMonthsParameter) orElse
      isValidMonthRange(cronMonthsParameter) orElse
      isValidStepValue(cronMonthsParameter) orElse
      isValidStepValueWithRange(cronMonthsParameter) orElse
      isValidMonthList(cronMonthsParameter)

  private def isValidMonthValue(string: String): Either[CronParsingError, CronFixedMonths] =
    string
      .toIntOption
      .toRight(left = InvalidMonthsString)
      .filterOrElse(
        months => months >= 1 && months < 12,
        InvalidMonthsString
      )
      .orElse(
        Either.cond(
          test = months.contains(string),
          right = months.indexOf(string)+1,
          left = InvalidMonthsString
        )
      )
      .map(CronFixedMonths.apply)

  private def isValidMonthRange(string: String): ValidatedMonths[CronMonthsRange] =
    string.split("-").toList match {
      case start :: end :: Nil =>
        for {
          validatedStart <- isValidMonthValue(start)
          validatedEnd <- isValidMonthValue(end)
        } yield CronMonthsRange(validatedStart.value, validatedEnd.value)
      case _ => Left(InvalidMonthsString)
    }

  private def isWildcard(string: String): ValidatedMonths[CronMonthsWildcard] =
    Either.cond(
      test = string == "*",
      right = CronMonthsWildcard(),
      left = InvalidMonthsString,
    )

  private def isValidStepValueWithRange(string: String): ValidatedMonths[CronMonthsStepsWithRange] =
    string.split("/").toList match {
      case range :: step :: Nil =>
        for {
          validatedRange <- isValidMonthRange(range)
          validatedStep <- isValidMonthValue(step)
        } yield CronMonthsStepsWithRange(validatedRange, validatedStep.value)
      case _ => Left(InvalidMonthsString)
    }

  private def isValidStepValue(string: String): ValidatedMonths[CronMonthsSteps] =
    string.split("/").toList match {
      case range :: step :: Nil =>
        for {
          _ <- isWildcard(range)
          validatedStep <- isValidMonthValue(step)
        } yield CronMonthsSteps(validatedStep.value)
      case _ => Left(InvalidMonthsString)
    }

  private def isValidMonthList(string: String): ValidatedMonths[CronMonthsList] =
    string.split(",")
      .toList
      .map(isValidMonthValue)
      .sequence
      .filterOrElse(
        _ => !string.endsWith(","), //this prevents input such as "1," or "," from being valid
        InvalidMonthsString
      )
      .map(list => CronMonthsList(list.map(_.value)))


}
