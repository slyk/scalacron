package dev.slyk
package cron.model

import utils.sequence

sealed trait CronDaysOfWeek

case class CronDaysOfWeekWildcard() extends CronDaysOfWeek

case class CronFixedDaysOfWeek(value: Int) extends CronDaysOfWeek

case class CronDaysOfWeekList(values: List[Int]) extends CronDaysOfWeek

case class CronDaysOfWeekRange(start: Int, end: Int) extends CronDaysOfWeek

case class CronDaysOfWeekSteps(step: Int) extends CronDaysOfWeek

case class CronDaysOfWeekStepsWithRange(range: CronDaysOfWeekRange, step: Int) extends CronDaysOfWeek

object CronDaysOfWeek {

  type ValidatedDaysOfWeek[A <: CronDaysOfWeek] = Either[CronParsingError, A]

  private val daysOfWeek = List("sun", "mon", "tue", "wed", "thu", "fri", "sat")

  def apply(cronDaysOfWeekParameter: String): ValidatedDaysOfWeek[CronDaysOfWeek] =
    isWildcard(cronDaysOfWeekParameter) orElse
      isValidDayOfWeekValue(cronDaysOfWeekParameter) orElse
      isValidDayOfWeekRange(cronDaysOfWeekParameter) orElse
      isValidStepValue(cronDaysOfWeekParameter) orElse
      isValidStepValueWithRange(cronDaysOfWeekParameter) orElse
      isValidDayOfWeekList(cronDaysOfWeekParameter)

  private def isValidDayOfWeekValue(string: String): Either[CronParsingError, CronFixedDaysOfWeek] =
    string
      .toIntOption
      .toRight(left = InvalidDaysOfWeekString)
      .filterOrElse(
        daysOfWeek => daysOfWeek >= 0 && daysOfWeek < 7,
        InvalidDaysOfWeekString
      )
      .orElse(
        Either.cond(
          test = daysOfWeek.contains(string),
          right = daysOfWeek.indexOf(string),
          left = InvalidDaysOfWeekString
        )
      )
      .map(CronFixedDaysOfWeek.apply)

  private def isValidDayOfWeekRange(string: String): ValidatedDaysOfWeek[CronDaysOfWeekRange] =
    string.split("-").toList match {
      case start :: end :: Nil =>
        for {
          validatedStart <- isValidDayOfWeekValue(start)
          validatedEnd <- isValidDayOfWeekValue(end)
        } yield CronDaysOfWeekRange(validatedStart.value, validatedEnd.value)
      case _ => Left(InvalidDaysOfWeekString)
    }

  private def isWildcard(string: String): ValidatedDaysOfWeek[CronDaysOfWeekWildcard] =
    Either.cond(
      test = string == "*",
      right = CronDaysOfWeekWildcard(),
      left = InvalidDaysOfWeekString,
    )

  private def isValidStepValueWithRange(string: String): ValidatedDaysOfWeek[CronDaysOfWeekStepsWithRange] =
    string.split("/").toList match {
      case range :: step :: Nil =>
        for {
          validatedRange <- isValidDayOfWeekRange(range)
          validatedStep <- isValidDayOfWeekValue(step)
        } yield CronDaysOfWeekStepsWithRange(validatedRange, validatedStep.value)
      case _ => Left(InvalidDaysOfWeekString)
    }

  private def isValidStepValue(string: String): ValidatedDaysOfWeek[CronDaysOfWeekSteps] =
    string.split("/").toList match {
      case range :: step :: Nil =>
        for {
          _ <- isWildcard(range)
          validatedStep <- isValidDayOfWeekValue(step)
        } yield CronDaysOfWeekSteps(validatedStep.value)
      case _ => Left(InvalidDaysOfWeekString)
    }

  private def isValidDayOfWeekList(string: String): ValidatedDaysOfWeek[CronDaysOfWeekList] =
    string.split(",")
      .toList
      .map(isValidDayOfWeekValue)
      .sequence
      .filterOrElse(
        _ => !string.endsWith(","), //this prevents input such as "1," or "," from being valid
        InvalidDaysOfWeekString
      )
      .map(list => CronDaysOfWeekList(list.map(_.value)))
  
}
