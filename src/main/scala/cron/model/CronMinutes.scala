package dev.slyk
package cron.model

import utils.sequence

sealed trait CronMinutes

case class CronMinutesWildcard() extends CronMinutes

case class CronFixedMinutes(value: Int) extends CronMinutes

case class CronMinutesList(values: List[Int]) extends CronMinutes

case class CronMinutesRange(start: Int, end: Int) extends CronMinutes

case class CronMinutesSteps(step: Int) extends CronMinutes

case class CronMinutesStepsWithRange(range: CronMinutesRange, step: Int) extends CronMinutes

object CronMinutes {

  type ValidatedMinutes[A <: CronMinutes] = Either[CronParsingError, A]

  def apply(cronMinutesParameter: String): ValidatedMinutes[CronMinutes] =
    isWildcard(cronMinutesParameter) orElse
      isValidMinuteValue(cronMinutesParameter) orElse
      isValidMinuteRange(cronMinutesParameter) orElse
      isValidStepValue(cronMinutesParameter) orElse
      isValidStepValueWithRange(cronMinutesParameter) orElse
      isValidMinuteList(cronMinutesParameter)

  private def isValidMinuteValue(string: String): Either[CronParsingError, CronFixedMinutes] =
    string
      .toIntOption
      .toRight(left = InvalidMinutesString)
      .filterOrElse(
        minutes => minutes >= 0 && minutes < 60,
        InvalidMinutesString
      )
      .map(CronFixedMinutes.apply)

  private def isValidMinuteRange(string: String): ValidatedMinutes[CronMinutesRange] =
    string.split("-").toList match {
      case start :: end :: Nil =>
        for {
          validatedStart <- isValidMinuteValue(start)
          validatedEnd <- isValidMinuteValue(end)
        } yield CronMinutesRange(validatedStart.value, validatedEnd.value)
      case _ => Left(InvalidMinutesString)
    }

  private def isWildcard(string: String): ValidatedMinutes[CronMinutesWildcard] =
    Either.cond(
      test = string == "*",
      right = CronMinutesWildcard(),
      left = InvalidMinutesString,
    )

  private def isValidStepValueWithRange(string: String): ValidatedMinutes[CronMinutesStepsWithRange] =
    string.split("/").toList match {
      case range :: step :: Nil =>
        for {
          validatedRange <- isValidMinuteRange(range)
          validatedStep <- isValidMinuteValue(step)
        } yield CronMinutesStepsWithRange(validatedRange, validatedStep.value)
      case _ => Left(InvalidMinutesString)
    }

  private def isValidStepValue(string: String): ValidatedMinutes[CronMinutesSteps] =
    string.split("/").toList match {
      case range :: step :: Nil =>
        for {
          _ <- isWildcard(range)
          validatedStep <- isValidMinuteValue(step)
        } yield CronMinutesSteps(validatedStep.value)
      case _ => Left(InvalidMinutesString)
    }

  private def isValidMinuteList(string: String): ValidatedMinutes[CronMinutesList] =
    string.split(",")
      .toList
      .map(isValidMinuteValue)
      .sequence
      .filterOrElse(
        _ => !string.endsWith(","), //this prevents input such as "1," or "," from being valid
        InvalidMinutesString
      )
      .map(list => CronMinutesList(list.map(_.value)))

}
