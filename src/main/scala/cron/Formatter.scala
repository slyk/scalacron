package dev.slyk.cron

import dev.slyk.cron.model.*

object Formatter {

  def prettyFormat(cronExpr: CronExpression): String =
    s"""
       |minute        ${formatMinutes(cronExpr.minutes)}
       |hour          ${formatHours(cronExpr.hours)}
       |day of month  ${formatDaysOfMonth(cronExpr.daysOfMonth)}
       |month         ${formatMonths(cronExpr.months)}
       |day of week   ${formatDaysOfWeek(cronExpr.daysOfWeek)}
       |command       ${cronExpr.command}
       |""".stripMargin

  private def formatMinutes(cronMinutes: CronMinutes): String =
    cronMinutes match {
      case minutes: CronMinutesWildcard => (0 to 59).mkString(" ")
      case minutes: CronFixedMinutes => minutes.value.toString
      case minutes: CronMinutesList => minutes.values.mkString(" ")
      case minutes: CronMinutesRange => (minutes.start to minutes.end).mkString(" ")
      case minutes: CronMinutesSteps => (0 to (59, minutes.step)).mkString(" ")
      case minutes: CronMinutesStepsWithRange => (minutes.range.start to (minutes.range.end, minutes.step)).mkString(" ")
    }

  private def formatHours(cronHours: CronHours): String =
    cronHours match {
      case hours: CronHoursWildcard => (0 to 23).mkString(" ")
      case hours: CronFixedHours => hours.value.toString
      case hours: CronHoursList => hours.values.mkString(" ")
      case hours: CronHoursRange => (hours.start to hours.end).mkString(" ")
      case hours: CronHoursSteps => (0 to (59, hours.step)).mkString(" ")
      case hours: CronHoursStepsWithRange => (hours.range.start to (hours.range.end, hours.step)).mkString(" ")
    }

  private def formatDaysOfMonth(daysOfMonth: CronDaysOfMonth): String =
    daysOfMonth match {
      case daysOfMonth: CronDaysOfMonthWildcard => (1 to 31).mkString(" ")
      case daysOfMonth: CronFixedDaysOfMonth => daysOfMonth.value.toString
      case daysOfMonth: CronDaysOfMonthList => daysOfMonth.values.mkString(" ")
      case daysOfMonth: CronDaysOfMonthRange => (daysOfMonth.start to daysOfMonth.end).mkString(" ")
      case daysOfMonth: CronDaysOfMonthSteps => (1 to (31, daysOfMonth.step)).mkString(" ")
      case daysOfMonth: CronDaysOfMonthStepsWithRange => (daysOfMonth.range.start to (daysOfMonth.range.end, daysOfMonth.step)).mkString(" ")
    }

  private def formatMonths(months: CronMonths): String =
    months match {
      case months: CronMonthsWildcard => (1 to 12).mkString(" ")
      case months: CronFixedMonths => months.value.toString
      case months: CronMonthsList => months.values.mkString(" ")
      case months: CronMonthsRange => (months.start to months.end).mkString(" ")
      case months: CronMonthsSteps => (1 to (12, months.step)).mkString(" ")
      case months: CronMonthsStepsWithRange => (months.range.start to (months.range.end, months.step)).mkString(" ")
    }

  private def formatDaysOfWeek(daysOfWeek: CronDaysOfWeek): String =
    daysOfWeek match {
      case daysOfWeek: CronDaysOfWeekWildcard => (0 to 6).mkString(" ")
      case daysOfWeek: CronFixedDaysOfWeek => daysOfWeek.value.toString
      case daysOfWeek: CronDaysOfWeekList => daysOfWeek.values.mkString(" ")
      case daysOfWeek: CronDaysOfWeekRange =>
        if (daysOfWeek.start < daysOfWeek.end) (daysOfWeek.start to daysOfWeek.end).mkString(" ")
        else (daysOfWeek.start to (daysOfWeek.end + 6)).map(_ % 7).mkString(" ")
      case daysOfWeek: CronDaysOfWeekSteps => (0 to (6, daysOfWeek.step)).mkString(" ")
      case daysOfWeek: CronDaysOfWeekStepsWithRange => (daysOfWeek.range.start to (daysOfWeek.range.end, daysOfWeek.step)).mkString(" ")
    }

}
