package dev.slyk
package cron.model

import munit.FunSuite

class CronMinutesTest extends FunSuite {
  test("CronMinutes parses a simple 15 minutes value correctly") {
    CronMinutes("15") match {
      case Left(_) => fail("Failed to parse simple Cron minutes value")
      case Right(minutes: CronFixedMinutes) =>
        assertEquals(minutes.value, 15, "Simple Cron minutes value was not equal to the input")
      case Right(_) => fail("Simple Cron minutes value did not match the expected type")
    }
  }

  test("CronMinutes parses a wildcard correctly") {
    CronMinutes("*") match {
      case Left(_) => fail("Failed to parse Cron wildcard")
      case Right(wildcard: CronMinutesWildcard) => assert(true)
      case Right(_) => fail("Cron wildcard was not parsed into the expected type")
    }
  }

  test("CronMinutes parses a list of minutes") {
    CronMinutes("1,2,3") match {
      case Left(_) => fail("Failed to parse Cron list of minutes")
      case Right(list: CronMinutesList) =>
        assertEquals(list.values, List(1, 2, 3), "Cron list of minutes did not match input")
      case Right(_) => fail("Cron list of minutes was not parsed into the expected type")
    }
  }

  test("CronMinutes parses a range of minutes") {
    CronMinutes("1-6") match {
      case Left(_) => fail("Failed to parse Cron range of minutes")
      case Right(range: CronMinutesRange) =>
        assertEquals(range.start, 1, "Cron range of minutes start did not match input")
        assertEquals(range.end, 6, "Cron range of minutes end did not match input")
      case Right(_) => fail("Cron range of minutes was not parsed into the expected type")
    }
  }

  test("CronMinutes parses a step of minutes with a wildcard") {
    CronMinutes("*/5") match {
      case Left(_) => fail("Failed to parse Cron steps of minutes")
      case Right(steps: CronMinutesSteps) =>
        assertEquals(steps.step, 5, "Cron steps of minutes did not match input")
      case Right(_) => fail("Cron steps of minutes was not parsed into the expected type")
    }
  }

  test("CronMinutes parses a step of minutes with a range") {
    CronMinutes("10-55/5") match {
      case Left(_) => fail("Failed to parse Cron steps of minutes")
      case Right(stepsWithRange: CronMinutesStepsWithRange) =>
        assertEquals(stepsWithRange.step, 5, "Cron steps of minutes' step did not match input")
        assertEquals(stepsWithRange.range.start, 10, "Cron steps of minutes' range start did not match input")
        assertEquals(stepsWithRange.range.end, 55, "Cron steps of minutes' range end did not match input")
      case Right(_) => fail("Cron steps of minutes with range was not parsed into the expected type")
    }
  }

  test("CronMinutes returns InvalidMinutesString for a number below 0") {
    CronMinutes("-10") match {
      case Left(error) => assertEquals(error, InvalidMinutesString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronMinutes returns InvalidMinutesString for a number above 59") {
    CronMinutes("60") match {
      case Left(error) => assertEquals(error, InvalidMinutesString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronMinutes returns InvalidMinutesString for an invalid cron minutes string") {
    CronMinutes("oof") match {
      case Left(error) => assertEquals(error, InvalidMinutesString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronMinutes returns InvalidMinutesString for a malformed range string") {
    CronMinutes("1-") match {
      case Left(error) => assertEquals(error, InvalidMinutesString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronMinutes returns InvalidMinutesString for a malformed list string") {
    CronMinutes("1,") match {
      case Left(error) => assertEquals(error, InvalidMinutesString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronMinutes returns InvalidMinutesString for a comma (instead of creating an empty list)") {
    CronMinutes(",") match {
      case Left(error) => assertEquals(error, InvalidMinutesString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronMinutes returns InvalidMinutesString for an empty string") {
    CronMinutes("") match {
      case Left(error) => assertEquals(error, InvalidMinutesString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

}
