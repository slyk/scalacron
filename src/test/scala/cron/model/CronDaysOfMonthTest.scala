package dev.slyk
package cron.model

import munit.FunSuite

class CronDaysOfMonthTest extends FunSuite {
  test("CronDaysOfMonth parses a simple 15th day of month value correctly") {
    CronDaysOfMonth("15") match {
      case Left(_) => fail("Failed to parse simple Cron days of month value")
      case Right(daysOfMonth: CronFixedDaysOfMonth) =>
        assertEquals(daysOfMonth.value, 15, "Simple Cron days of month value was not equal to the input")
      case Right(_) => fail("Simple Cron days of month value did not match the expected type")
    }
  }

  test("CronDaysOfMonth parses a wildcard correctly") {
    CronDaysOfMonth("*") match {
      case Left(_) => fail("Failed to parse Cron wildcard")
      case Right(wildcard: CronDaysOfMonthWildcard) => assert(true)
      case Right(_) => fail("Cron wildcard was not parsed into the expected type")
    }
  }

  test("CronDaysOfMonth parses a list of days of month") {
    CronDaysOfMonth("1,2,3") match {
      case Left(_) => fail("Failed to parse Cron list of days of month")
      case Right(list: CronDaysOfMonthList) =>
        assertEquals(list.values, List(1, 2, 3), "Cron list of days of month did not match input")
      case Right(_) => fail("Cron list of days of month was not parsed into the expected type")
    }
  }

  test("CronDaysOfMonth parses a range of days of month") {
    CronDaysOfMonth("1-6") match {
      case Left(_) => fail("Failed to parse Cron range of days of month")
      case Right(range: CronDaysOfMonthRange) =>
        assertEquals(range.start, 1, "Cron range of days of month start did not match input")
        assertEquals(range.end, 6, "Cron range of days of month end did not match input")
      case Right(_) => fail("Cron range of days of month was not parsed into the expected type")
    }
  }

  test("CronDaysOfMonth parses a step of days of month with a wildcard") {
    CronDaysOfMonth("*/5") match {
      case Left(_) => fail("Failed to parse Cron steps of days of month")
      case Right(steps: CronDaysOfMonthSteps) =>
        assertEquals(steps.step, 5, "Cron steps of days of month did not match input")
      case Right(_) => fail("Cron steps of days of month was not parsed into the expected type")
    }
  }

  test("CronDaysOfMonth parses a step of days of month with a range") {
    CronDaysOfMonth("2-28/2") match {
      case Left(_) => fail("Failed to parse Cron steps of days of month")
      case Right(stepsWithRange: CronDaysOfMonthStepsWithRange) =>
        assertEquals(stepsWithRange.step, 2, "Cron steps of days of month' step did not match input")
        assertEquals(stepsWithRange.range.start, 2, "Cron steps of days of month' range start did not match input")
        assertEquals(stepsWithRange.range.end, 28, "Cron steps of days of month' range end did not match input")
      case Right(_) => fail("Cron steps of days of month with range was not parsed into the expected type")
    }
  }

  test("CronDaysOfMonth returns DaysOfMonthOutOfBounds for a number below 1") {
    CronDaysOfMonth("0") match {
      case Left(error) => assertEquals(error, InvalidDaysOfMonthString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronDaysOfMonth returns DaysOfMonthOutOfBounds for a number above 31") {
    CronDaysOfMonth("32") match {
      case Left(error) => assertEquals(error, InvalidDaysOfMonthString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronDaysOfMonth returns InvalidDaysOfMonthString for an invalid cron days of month string") {
    CronDaysOfMonth("oof") match {
      case Left(error) => assertEquals(error, InvalidDaysOfMonthString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronDaysOfMonth returns InvalidDaysOfMonthString for a malformed range string") {
    CronDaysOfMonth("1-") match {
      case Left(error) => assertEquals(error, InvalidDaysOfMonthString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronDaysOfMonth returns InvalidDaysOfMonthString for a malformed list string") {
    CronDaysOfMonth("1,") match {
      case Left(error) => assertEquals(error, InvalidDaysOfMonthString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronDaysOfMonth returns InvalidDaysOfMonthString for a comma (instead of creating an empty list)") {
    CronDaysOfMonth(",") match {
      case Left(error) => assertEquals(error, InvalidDaysOfMonthString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronDaysOfMonth returns InvalidDaysOfMonthString for an empty string") {
    CronDaysOfMonth("") match {
      case Left(error) => assertEquals(error, InvalidDaysOfMonthString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

}
