package dev.slyk
package cron.model

import munit.FunSuite

class CronDaysOfWeekTest extends FunSuite {
  test("CronDaysOfWeek parses a simple 3rd day of week value correctly") {
    CronDaysOfWeek("3") match {
      case Left(_) => fail("Failed to parse simple Cron days of week value")
      case Right(daysOfWeek: CronFixedDaysOfWeek) =>
        assertEquals(daysOfWeek.value, 3, "Simple Cron days of week value was not equal to the input")
      case Right(_) => fail("Simple Cron days of week value did not match the expected type")
    }
  }

  test("CronDaysOfWeek parses a simple \"mon\" as day of week value correctly") {
    CronDaysOfWeek("mon") match {
      case Left(_) => fail("Failed to parse simple Cron days of week value")
      case Right(daysOfWeek: CronFixedDaysOfWeek) =>
        assertEquals(daysOfWeek.value, 1, "Simple Cron days of week value was not equal to the input")
      case Right(_) => fail("Simple Cron days of week value did not match the expected type")
    }
  }

  test("CronDaysOfWeek parses a simple \"tue\" as day of week value correctly") {
    CronDaysOfWeek("tue") match {
      case Left(_) => fail("Failed to parse simple Cron days of week value")
      case Right(daysOfWeek: CronFixedDaysOfWeek) =>
        assertEquals(daysOfWeek.value, 2, "Simple Cron days of week value was not equal to the input")
      case Right(_) => fail("Simple Cron days of week value did not match the expected type")
    }
  }

  test("CronDaysOfWeek parses a simple \"wed\" as day of week value correctly") {
    CronDaysOfWeek("wed") match {
      case Left(_) => fail("Failed to parse simple Cron days of week value")
      case Right(daysOfWeek: CronFixedDaysOfWeek) =>
        assertEquals(daysOfWeek.value, 3, "Simple Cron days of week value was not equal to the input")
      case Right(_) => fail("Simple Cron days of week value did not match the expected type")
    }
  }

  test("CronDaysOfWeek parses a simple \"thu\" as day of week value correctly") {
    CronDaysOfWeek("thu") match {
      case Left(_) => fail("Failed to parse simple Cron days of week value")
      case Right(daysOfWeek: CronFixedDaysOfWeek) =>
        assertEquals(daysOfWeek.value, 4, "Simple Cron days of week value was not equal to the input")
      case Right(_) => fail("Simple Cron days of week value did not match the expected type")
    }
  }

  test("CronDaysOfWeek parses a simple \"fri\" as day of week value correctly") {
    CronDaysOfWeek("fri") match {
      case Left(_) => fail("Failed to parse simple Cron days of week value")
      case Right(daysOfWeek: CronFixedDaysOfWeek) =>
        assertEquals(daysOfWeek.value, 5, "Simple Cron days of week value was not equal to the input")
      case Right(_) => fail("Simple Cron days of week value did not match the expected type")
    }
  }

  test("CronDaysOfWeek parses a simple \"sat\" as day of week value correctly") {
    CronDaysOfWeek("sat") match {
      case Left(_) => fail("Failed to parse simple Cron days of week value")
      case Right(daysOfWeek: CronFixedDaysOfWeek) =>
        assertEquals(daysOfWeek.value, 6, "Simple Cron days of week value was not equal to the input")
      case Right(_) => fail("Simple Cron days of week value did not match the expected type")
    }
  }

  test("CronDaysOfWeek parses a simple \"sun\" as day of week value correctly") {
    CronDaysOfWeek("sun") match {
      case Left(_) => fail("Failed to parse simple Cron days of week value")
      case Right(daysOfWeek: CronFixedDaysOfWeek) =>
        assertEquals(daysOfWeek.value, 0, "Simple Cron days of week value was not equal to the input")
      case Right(_) => fail("Simple Cron days of week value did not match the expected type")
    }
  }

  test("CronDaysOfWeek parses a wildcard correctly") {
    CronDaysOfWeek("*") match {
      case Left(_) => fail("Failed to parse Cron wildcard")
      case Right(wildcard: CronDaysOfWeekWildcard) => assert(true)
      case Right(_) => fail("Cron wildcard was not parsed into the expected type")
    }
  }

  test("CronDaysOfWeek parses a list of days of week") {
    CronDaysOfWeek("1,2,3") match {
      case Left(_) => fail("Failed to parse Cron list of days of week")
      case Right(list: CronDaysOfWeekList) =>
        assertEquals(list.values, List(1, 2, 3), "Cron list of days of week did not match input")
      case Right(_) => fail("Cron list of days of week was not parsed into the expected type")
    }
  }

  test("CronDaysOfWeek parses a range of days of week") {
    CronDaysOfWeek("1-5") match {
      case Left(_) => fail("Failed to parse Cron range of days of week")
      case Right(range: CronDaysOfWeekRange) =>
        assertEquals(range.start, 1, "Cron range of days of week start did not match input")
        assertEquals(range.end, 5, "Cron range of days of week end did not match input")
      case Right(_) => fail("Cron range of days of week was not parsed into the expected type")
    }
  }

  test("CronDaysOfWeek parses a step of days of week with a wildcard") {
    CronDaysOfWeek("*/5") match {
      case Left(_) => fail("Failed to parse Cron steps of days of week")
      case Right(steps: CronDaysOfWeekSteps) =>
        assertEquals(steps.step, 5, "Cron steps of days of week did not match input")
      case Right(_) => fail("Cron steps of days of week was not parsed into the expected type")
    }
  }

  test("CronDaysOfWeek parses a step of days of week with a range") {
    CronDaysOfWeek("1-5/2") match {
      case Left(_) => fail("Failed to parse Cron steps of days of week")
      case Right(stepsWithRange: CronDaysOfWeekStepsWithRange) =>
        assertEquals(stepsWithRange.step, 2, "Cron steps of days of week' step did not match input")
        assertEquals(stepsWithRange.range.start, 1, "Cron steps of days of week' range start did not match input")
        assertEquals(stepsWithRange.range.end, 5, "Cron steps of days of week' range end did not match input")
      case Right(_) => fail("Cron steps of days of week with range was not parsed into the expected type")
    }
  }

  test("CronDaysOfWeek returns DaysOfWeekOutOfBounds for a number below 0") {
    CronDaysOfWeek("-7") match {
      case Left(error) => assertEquals(error, InvalidDaysOfWeekString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronDaysOfWeek returns DaysOfWeekOutOfBounds for a number above 7") {
    CronDaysOfWeek("8") match {
      case Left(error) => assertEquals(error, InvalidDaysOfWeekString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronDaysOfWeek returns InvalidDaysOfWeekString for an invalid cron days of week string") {
    CronDaysOfWeek("oof") match {
      case Left(error) => assertEquals(error, InvalidDaysOfWeekString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronDaysOfWeek returns InvalidDaysOfWeekString for a malformed range string") {
    CronDaysOfWeek("1-") match {
      case Left(error) => assertEquals(error, InvalidDaysOfWeekString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronDaysOfWeek returns InvalidDaysOfWeekString for a malformed list string") {
    CronDaysOfWeek("1,") match {
      case Left(error) => assertEquals(error, InvalidDaysOfWeekString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronDaysOfWeek returns InvalidDaysOfWeekString for a comma (instead of creating an empty list)") {
    CronDaysOfWeek(",") match {
      case Left(error) => assertEquals(error, InvalidDaysOfWeekString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronDaysOfWeek returns InvalidDaysOfWeekString for an empty string") {
    CronDaysOfWeek("") match {
      case Left(error) => assertEquals(error, InvalidDaysOfWeekString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

}
