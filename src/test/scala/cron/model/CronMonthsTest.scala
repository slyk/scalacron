package dev.slyk
package cron.model

import munit.FunSuite

class CronMonthsTest extends FunSuite {
  test("CronMonths parses a simple 2nd month value correctly") {
    CronMonths("2") match {
      case Left(_) => fail("Failed to parse simple Cron months value")
      case Right(months: CronFixedMonths) =>
        assertEquals(months.value, 2, "Simple Cron months value was not equal to the input")
      case Right(_) => fail("Simple Cron months value did not match the expected type")
    }
  }

  test("CronMonths parses a simple \"jan\" as month value correctly") {
    CronMonths("jan") match {
      case Left(_) => fail("Failed to parse simple Cron months value")
      case Right(months: CronFixedMonths) =>
        assertEquals(months.value, 1, "Simple Cron months value was not equal to the input")
      case Right(_) => fail("Simple Cron months value did not match the expected type")
    }
  }

  test("CronMonths parses a simple \"feb\" as month value correctly") {
    CronMonths("feb") match {
      case Left(_) => fail("Failed to parse simple Cron months value")
      case Right(months: CronFixedMonths) =>
        assertEquals(months.value, 2, "Simple Cron months value was not equal to the input")
      case Right(_) => fail("Simple Cron months value did not match the expected type")
    }
  }

  test("CronMonths parses a simple \"mar\" as month value correctly") {
    CronMonths("mar") match {
      case Left(_) => fail("Failed to parse simple Cron months value")
      case Right(months: CronFixedMonths) =>
        assertEquals(months.value, 3, "Simple Cron months value was not equal to the input")
      case Right(_) => fail("Simple Cron months value did not match the expected type")
    }
  }

  test("CronMonths parses a simple \"apr\" as month value correctly") {
    CronMonths("apr") match {
      case Left(_) => fail("Failed to parse simple Cron months value")
      case Right(months: CronFixedMonths) =>
        assertEquals(months.value, 4, "Simple Cron months value was not equal to the input")
      case Right(_) => fail("Simple Cron months value did not match the expected type")
    }
  }

  test("CronMonths parses a simple \"may\" as month value correctly") {
    CronMonths("may") match {
      case Left(_) => fail("Failed to parse simple Cron months value")
      case Right(months: CronFixedMonths) =>
        assertEquals(months.value, 5, "Simple Cron months value was not equal to the input")
      case Right(_) => fail("Simple Cron months value did not match the expected type")
    }
  }

  test("CronMonths parses a simple \"jun\" as month value correctly") {
    CronMonths("jun") match {
      case Left(_) => fail("Failed to parse simple Cron months value")
      case Right(months: CronFixedMonths) =>
        assertEquals(months.value, 6, "Simple Cron months value was not equal to the input")
      case Right(_) => fail("Simple Cron months value did not match the expected type")
    }
  }

  test("CronMonths parses a simple \"jul\" as month value correctly") {
    CronMonths("jul") match {
      case Left(_) => fail("Failed to parse simple Cron months value")
      case Right(months: CronFixedMonths) =>
        assertEquals(months.value, 7, "Simple Cron months value was not equal to the input")
      case Right(_) => fail("Simple Cron months value did not match the expected type")
    }
  }

  test("CronMonths parses a simple \"aug\" as month value correctly") {
    CronMonths("aug") match {
      case Left(_) => fail("Failed to parse simple Cron months value")
      case Right(months: CronFixedMonths) =>
        assertEquals(months.value, 8, "Simple Cron months value was not equal to the input")
      case Right(_) => fail("Simple Cron months value did not match the expected type")
    }
  }

  test("CronMonths parses a simple \"sep\" as month value correctly") {
    CronMonths("sep") match {
      case Left(_) => fail("Failed to parse simple Cron months value")
      case Right(months: CronFixedMonths) =>
        assertEquals(months.value, 9, "Simple Cron months value was not equal to the input")
      case Right(_) => fail("Simple Cron months value did not match the expected type")
    }
  }

  test("CronMonths parses a simple \"oct\" as month value correctly") {
    CronMonths("oct") match {
      case Left(_) => fail("Failed to parse simple Cron months value")
      case Right(months: CronFixedMonths) =>
        assertEquals(months.value, 10, "Simple Cron months value was not equal to the input")
      case Right(_) => fail("Simple Cron months value did not match the expected type")
    }
  }

  test("CronMonths parses a simple \"nov\" as month value correctly") {
    CronMonths("nov") match {
      case Left(_) => fail("Failed to parse simple Cron months value")
      case Right(months: CronFixedMonths) =>
        assertEquals(months.value, 11, "Simple Cron months value was not equal to the input")
      case Right(_) => fail("Simple Cron months value did not match the expected type")
    }
  }

  test("CronMonths parses a simple \"dec\" as month value correctly") {
    CronMonths("dec") match {
      case Left(_) => fail("Failed to parse simple Cron months value")
      case Right(months: CronFixedMonths) =>
        assertEquals(months.value, 12, "Simple Cron months value was not equal to the input")
      case Right(_) => fail("Simple Cron months value did not match the expected type")
    }
  }

  test("CronMonths parses a wildcard correctly") {
    CronMonths("*") match {
      case Left(_) => fail("Failed to parse Cron wildcard")
      case Right(_: CronMonthsWildcard) => assert(true)
      case Right(_) => fail("Cron wildcard was not parsed into the expected type")
    }
  }

  test("CronMonths parses a list of months") {
    CronMonths("1,2,3") match {
      case Left(_) => fail("Failed to parse Cron list of months")
      case Right(list: CronMonthsList) =>
        assertEquals(list.values, List(1, 2, 3), "Cron list of months did not match input")
      case Right(_) => fail("Cron list of months was not parsed into the expected type")
    }
  }

  test("CronMonths parses a range of months") {
    CronMonths("1-8") match {
      case Left(_) => fail("Failed to parse Cron range of months")
      case Right(range: CronMonthsRange) =>
        assertEquals(range.start, 1, "Cron range of months start did not match input")
        assertEquals(range.end, 8, "Cron range of months end did not match input")
      case Right(_) => fail("Cron range of months was not parsed into the expected type")
    }
  }

  test("CronMonths parses a step of months with a wildcard") {
    CronMonths("*/2") match {
      case Left(_) => fail("Failed to parse Cron steps of months")
      case Right(steps: CronMonthsSteps) =>
        assertEquals(steps.step, 2, "Cron steps of months did not match input")
      case Right(_) => fail("Cron steps of months was not parsed into the expected type")
    }
  }

  test("CronMonths parses a step of months with a range") {
    CronMonths("2-10/2") match {
      case Left(_) => fail("Failed to parse Cron steps of months")
      case Right(stepsWithRange: CronMonthsStepsWithRange) =>
        assertEquals(stepsWithRange.step, 2, "Cron steps of months' step did not match input")
        assertEquals(stepsWithRange.range.start, 2, "Cron steps of months' range start did not match input")
        assertEquals(stepsWithRange.range.end, 10, "Cron steps of months' range end did not match input")
      case Right(_) => fail("Cron steps of months with range was not parsed into the expected type")
    }
  }

  test("CronMonths returns MonthsOutOfBounds for a number below 1") {
    CronMonths("0") match {
      case Left(error) => assertEquals(error, InvalidMonthsString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronMonths returns MonthsOutOfBounds for a number above 12") {
    CronMonths("13") match {
      case Left(error) => assertEquals(error, InvalidMonthsString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronMonths returns InvalidMonthsString for an invalid cron months string") {
    CronMonths("oof") match {
      case Left(error) => assertEquals(error, InvalidMonthsString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronMonths returns InvalidMonthsString for a malformed range string") {
    CronMonths("1-") match {
      case Left(error) => assertEquals(error, InvalidMonthsString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronMonths returns InvalidMonthsString for a malformed list string") {
    CronMonths("1,") match {
      case Left(error) => assertEquals(error, InvalidMonthsString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronMonths returns InvalidMonthsString for a comma (instead of creating an empty list)") {
    CronMonths(",") match {
      case Left(error) => assertEquals(error, InvalidMonthsString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronMonths returns InvalidMonthsString for an empty string") {
    CronMonths("") match {
      case Left(error) => assertEquals(error, InvalidMonthsString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

}
