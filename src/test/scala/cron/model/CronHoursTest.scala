package dev.slyk
package cron.model

import munit.FunSuite

class CronHoursTest extends FunSuite {
  test("CronHours parses a simple 15 hours value correctly") {
    CronHours("15") match {
      case Left(_) => fail("Failed to parse simple Cron hours value")
      case Right(hours: CronFixedHours) =>
        assertEquals(hours.value, 15, "Simple Cron hours value was not equal to the input")
      case Right(_) => fail("Simple Cron hours value did not match the expected type")
    }
  }

  test("CronHours parses a wildcard correctly") {
    CronHours("*") match {
      case Left(_) => fail("Failed to parse Cron wildcard")
      case Right(wildcard: CronHoursWildcard) => assert(true)
      case Right(_) => fail("Cron wildcard was not parsed into the expected type")
    }
  }

  test("CronHours parses a list of hours") {
    CronHours("1,2,3") match {
      case Left(_) => fail("Failed to parse Cron list of hours")
      case Right(list: CronHoursList) =>
        assertEquals(list.values, List(1, 2, 3), "Cron list of hours did not match input")
      case Right(_) => fail("Cron list of hours was not parsed into the expected type")
    }
  }

  test("CronHours parses a range of hours") {
    CronHours("1-6") match {
      case Left(_) => fail("Failed to parse Cron range of hours")
      case Right(range: CronHoursRange) =>
        assertEquals(range.start, 1, "Cron range of hours start did not match input")
        assertEquals(range.end, 6, "Cron range of hours end did not match input")
      case Right(_) => fail("Cron range of hours was not parsed into the expected type")
    }
  }

  test("CronHours parses a step of hours with a wildcard") {
    CronHours("*/5") match {
      case Left(_) => fail("Failed to parse Cron steps of hours")
      case Right(steps: CronHoursSteps) =>
        assertEquals(steps.step, 5, "Cron steps of hours did not match input")
      case Right(_) => fail("Cron steps of hours was not parsed into the expected type")
    }
  }

  test("CronHours parses a step of hours with a range") {
    CronHours("1-10/2") match {
      case Left(_) => fail("Failed to parse Cron steps of hours")
      case Right(stepsWithRange: CronHoursStepsWithRange) =>
        assertEquals(stepsWithRange.step, 2, "Cron steps of hours' step did not match input")
        assertEquals(stepsWithRange.range.start, 1, "Cron steps of hours' range start did not match input")
        assertEquals(stepsWithRange.range.end, 10, "Cron steps of hours' range end did not match input")
      case Right(_) => fail("Cron steps of hours with range was not parsed into the expected type")
    }
  }

  test("CronHours returns HoursOutOfBounds for a number below 0") {
    CronHours("-10") match {
      case Left(error) => assertEquals(error, InvalidHoursString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronHours returns HoursOutOfBounds for a number above 23") {
    CronHours("24") match {
      case Left(error) => assertEquals(error, InvalidHoursString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronHours returns InvalidHoursString for an invalid cron hours string") {
    CronHours("oof") match {
      case Left(error) => assertEquals(error, InvalidHoursString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronHours returns InvalidHoursString for a malformed range string") {
    CronHours("1-") match {
      case Left(error) => assertEquals(error, InvalidHoursString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronHours returns InvalidHoursString for a malformed list string") {
    CronHours("1,") match {
      case Left(error) => assertEquals(error, InvalidHoursString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronHours returns InvalidHoursString for a comma (instead of creating an empty list)") {
    CronHours(",") match {
      case Left(error) => assertEquals(error, InvalidHoursString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

  test("CronHours returns InvalidHoursString for an empty string") {
    CronHours("") match {
      case Left(error) => assertEquals(error, InvalidHoursString, "Cron parsing failed with incorrect error")
      case Right(_) => fail("Cron did not produce a failure when expected")
    }
  }

}
